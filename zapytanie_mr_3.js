var mapFunction3 = function() {
 emit(this.job, 1);
};

var reduceFunction3 = function(key, values) {
	return key;
};

db.people.mapReduce(
	mapFunction3,
	reduceFunction3,
	{out: "zad3"}
)

printjson(db.zad3.find({},{_id: 1}).toArray());
