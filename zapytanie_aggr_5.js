balanceToDblStage = {
   $addFields: {
      balanceDbl: { $toDouble: "$credit.balance" }
   }
};

printjson(db.people.aggregate([
{$match: {"sex": "Female", "nationality": "Poland"}},
{$unwind: "$credit"},
balanceToDblStage,
{$group: {
	_id: "$credit.currency",
	"avarageBalance" : {"$avg" : "$balanceDbl"},
	"totalBalance" : {"$sum" : "$balanceDbl"}
}}
]).toArray());
