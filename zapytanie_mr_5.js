var mapFunction5 = function() {
	for (var idx = 0; idx < this.credit.length; idx++) {
												 var key = this.credit[idx].currency;
												 var value = {
												 count: 1,
												 balance: this.credit[idx].balance};
												 emit(key, value);
	 }
};

var reduceFunction5 = function(key, values) {
reducedVal = { count: 0, balance: 0};
	for (var idx = 0.0; idx < values.length; idx++) {
		reducedVal.count += values[idx].count;
		reducedVal.balance += parseFloat(values[idx].balance);
	}
	return reducedVal;
};

var finalizeFunction5 = function (key, reducedVal) {
		resValue = {
			avarageBalance : reducedVal.balance/ reducedVal.count,
			totalBalance: reducedVal.balance
		}
	return resValue;
};

db.people.mapReduce(
	mapFunction5,
	reduceFunction5,
	{out: "zad5", query : {"sex": "Female", "nationality": "Poland"}, finalize: finalizeFunction5}
);

printjson(db.zad5.find().toArray());
