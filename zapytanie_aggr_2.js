balanceDblStage = {
   $addFields: {
      balanceDbl: { $toDouble: "$credit.balance" }
   }
};
printjson(db.people.aggregate([
{$unwind: "$credit"},
balanceDblStage,
{$group: {
	_id: "$credit.currency",
	"totalBalance" : {"$sum" : "$balanceDbl"}
}}
]).toArray());
