printjson(db.people.aggregate({$group: {
	_id: "$sex",
	"meanHeight" : {"$avg" : "$convertedHeight"},
	"meanWeights" : {"$avg" : "$convertedWeight"}
}}).toArray());
