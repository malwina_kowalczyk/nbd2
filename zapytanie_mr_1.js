var mapFunction1 = function() {
	var key = this.sex;
	var value = {
		count: 1,
		convertedWeight: parseFloat(this.weight),
		convertedHeight: parseFloat(this.height)
		};
 emit(key, value);
};

var reduceFunction1 = function(key, values) {
reducedVal = { count: 0, convertedWeight: 0.0, convertedHeight: 0.0 };
	for (var idx = 0; idx < values.length; idx++) {
		reducedVal.count += values[idx].count;
		reducedVal.convertedWeight += values[idx].convertedWeight;
		reducedVal.convertedHeight += values[idx].convertedHeight;
	}
	return reducedVal;
};

var finalizeFunction1 = function (key, reducedVal) {
	var resVal = {
		meanHeight : reducedVal.convertedHeight/reducedVal.count,
		meanWeight : reducedVal.convertedWeight/reducedVal.count
	}
	return resVal;
};

db.people.mapReduce(
	mapFunction1,
	reduceFunction1,
	{out: "zad1", finalize: finalizeFunction1}
);

printjson(db.zad1.find().toArray());
