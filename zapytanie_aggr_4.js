addTemporaryBmiStage = {
   $addFields:
      {bmi: { $toDouble: { $divide: ["$convertedWeight", {$pow: [{$divide:["$convertedHeight",100]}, 2]}]}}}
};

printjson(db.people.aggregate([
addTemporaryBmiStage,
{$group: {
	_id: "$nationality",
	"minBmi" : {"$min" : "$bmi"},
	"avgBmi" : {"$avg" : "$bmi"},
	"maxBmi" : {"$max" : "$bmi"}
}}
]).toArray());
