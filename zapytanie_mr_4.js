var mapFunction4 = function() {
var key = this.nationality;
var value = {
	count: 1,
	bmi: parseFloat(this.weight)/(Math.pow(parseFloat(this.height)/100,2)),
	min: parseFloat(this.weight)/(Math.pow(parseFloat(this.height)/100,2)),
	max: parseFloat(this.weight)/(Math.pow(parseFloat(this.height)/100,2)),
	};
emit(key, value);
};

var reduceFunction4 = function(key, values) {
reducedVal = { count: 0, bmi: 0, min:Number.MAX_VALUE, max:0 };
	for (var idx = 0; idx < values.length; idx++) {
		reducedVal.count += values[idx].count;
		reducedVal.bmi += values[idx].bmi;
		reducedVal.max = Math.max(reducedVal.max, values[idx].max );
		reducedVal.min = Math.min(reducedVal.min, values[idx].min );
	}
	return reducedVal;
};

var finalizeFunction4 = function (key, reducedVal) {
	resVal = {
		minBmi : reducedVal.min,
		maxBmi : reducedVal.max,
		avgBmi: reducedVal.bmi/reducedVal.count
	}
	return resVal;
};


db.people.mapReduce(
	mapFunction4,
	reduceFunction4,
	{out: "zad4", finalize: finalizeFunction4}
	);

printjson(db.zad4.find().toArray());
